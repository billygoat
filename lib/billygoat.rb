['rubygems', 'activesupport', 'fileutils', 'yaml', 'pp'].each {|lib| require lib }
Dir.glob(File.expand_path(File.dirname(__FILE__) + '/**/*.rb')) do |lib|
  require lib
end