class ClockError < RuntimeError; end

class Day
  attr_accessor :times
  MustClockInFirst = "You must clock in first"

  def initialize
    @times = []
    @clocked_in = false
  end    
  
  def clock_in!(time_in=Time.now)
    returning(time_in) do
      if clocked_out?
        @times << TimeSpan.new(time_in)
      else
        current_timespan.start_time = time_in
      end      
      @clocked_in = true
    end  
  end
  
  def clock_out!(time_out=Time.now)
    raise ClockError.new(MustClockInFirst) unless current_timespan
    returning(time_out) do
      current_timespan.end_time = time_out
      @clocked_in = false
    end
  end
  
  def clocked_in?
    @clocked_in
  end
  
  def clocked_out?
    !clocked_in?
  end
  
  def clear!
    @times = []
  end
  
  def time_in
    @times.first.start_time
  end
  
  def time_out
    raise ClockError.new(MustClockInFirst) unless current_timespan
    current_timespan.end_time
  end
  
  def hours 
    @times.inject(0) {|total, span| total + span.length}.round
  end
  
  def items
    @times.collect {|span| span.items}.flatten.uniq
  end
  
  def current_timespan
    @times.last
  end

  def add_items(*items)
    raise ClockError.new(MustClockInFirst) unless current_timespan
    current_timespan.add_items(items)
  end
end