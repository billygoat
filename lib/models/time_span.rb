class TimeSpan
  attr_accessor :start_time, :end_time, :items
  
  def initialize(start_time=nil, end_time=nil, items=[])
    @start_time, @end_time, @items = start_time, end_time, items
  end
  
  def length
    return 0 if [start_time, end_time].any?(&:nil?)
    (end_time - start_time) / 3600
  end
  
  def add_items(*items)
    @items.concat(items)
    @items.flatten!
  end  
end