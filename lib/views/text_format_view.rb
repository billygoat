class TextFormatView
  ShortTime = "%I:%M %p"  
  ShortDate = "%a, %m %b %Y"
  OneLine = "\n"
  TwoLines = "\n\n"
  
  def format_time(time)
    time.strftime(ShortTime)
  end    
  
  def format_time_span(span)
    "Time in:\t#{format_time(span.start_time)}#{OneLine}" + 
    "Time out:\t#{format_time(span.end_time)}"
  end
  
  def format_day(day)
    [
      day.time_in.strftime(ShortDate),
      day.times.collect {|span| format_time_span(span)}.join(OneLine),
      day.items.collect {|item| "* #{item}"}.join(OneLine)      
    ].join(TwoLines)    
  end
end