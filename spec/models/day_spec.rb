require File.dirname(__FILE__) + '/../spec_helper'

describe Day, "when first created" do  
  before do
    @day = Day.new
  end
  
  it "should have an empty list of items" do
    @day.items.should be_empty
  end
  
  it "should have an empty list of time spans" do
    @day.times.should be_empty
  end    
  
  it "should have zero total hours" do
    @day.hours.should be_zero
  end
  
  it "should be clocked out" do
    @day.should be_clocked_out
  end
end

describe Day, "after clocking in" do
  before do 
    @day = Day.new
    @original_time_in = Time.now
    @day.clock_in!(@original_time_in)
  end
  
  it "should be clocked in" do
    @day.should be_clocked_in
  end
  
  it "should not create a new time span if you clock in again" do
    @day.clock_in!
    @day.times.should have(1).timespan
  end
  
  it "should update the time in when you clock in again" do
    @day.clock_in!
    @day.time_in.should be_after(@original_time_in)
  end
end

describe Day, "after clocking out" do
  before do 
    @day = Day.new
    @day.clock_in!
    @day.clock_out!
  end
  
  it "should be clocked out" do
    @day.should be_clocked_out
  end
end

describe Day, "when clocking in and out" do
  before do
    @hours = 8
    @time_in = Time.now
    @time_out = @time_in + @hours.hours
    @time_span = TimeSpan.new(@time_in, @time_out)
    Time.should_receive(:now).twice.and_return(@time_out)
    TimeSpan.should_receive(:new).and_return(@time_span)            
    @day = Day.new
  end
  
  it "should remember the time you clock in" do    
    @day.clock_in!
    @day.clock_out!
    @day.time_in.should == @time_in
  end
  
  it "should remember the time you clock out" do    
    @day.clock_in!
    @day.clock_out!
    @day.time_out.should == @time_out
  end
  
  it "should return the total hours worked" do
    @day.clock_in!
    @day.clock_out!
    @day.hours.should == @hours
  end
end

describe Day do
  before do
    @day = Day.new
    @now = Time.now
  end
  
  it "should not display duplicate items" do    
    @day.clock_in!
    3.times do
      @day.add_items("Ate", "Ate")
    end  
    @day.items.should have(1).item
  end
  
  it "should add items to the current time span" do
    work_item = "Worked on stuff"
    @day.clock_in!
    @day.add_items work_item    
    @day.current_timespan.items.should include(work_item)
  end
  
  it "should return the current timespan" do
    @day.current_timespan.should == @day.times.last
  end
  
  it "should allow you to specify a clock in time" do    
    @day.clock_in! @now
    @day.time_in.should == @now
  end
  
  it "should allow you to specify a clock out time" do
    @day.clock_in! @now
    @day.clock_out! @now
    @day.time_out.should == @now
  end
  
  it "should raise an error when trying to clock out before you clock in" do
    lambda { @day.clock_out! }.should raise_error(ClockError, Day::MustClockInFirst)
  end
  
  it "should allow you to clear all data" do
    @day.clock_in!
    @day.clock_out!
    @day.clear!
    @day.times.should have(0).items
    @day.items.should have(0).items
  end
end