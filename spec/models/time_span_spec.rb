require File.dirname(__FILE__) + '/../spec_helper'

describe TimeSpan, "when initialized with no times" do
  before do
    @time_span = TimeSpan.new
  end
  
  it "should have an empty list of work items" do
    @time_span.items.should have(0).work_items
  end
end

describe TimeSpan, "when initialized with only a start time" do
  before do
    @time_in = Time.new
    @time_span = TimeSpan.new(@time_in)
  end
  
  it "should remember the start time correctly" do
    @time_span.start_time.should == @time_in      
  end
  
  it "should return zero for the length" do
    @time_span.length.should be_zero
  end
end

describe TimeSpan, "when initialized with a start and end time" do
  before do
    @hours = 8
    @time_in = Time.new
    @time_out = @time_in + @hours.hours
    @time_span = TimeSpan.new(@time_in, @time_out)
  end
  
  it "should remember the end time correctly" do
    @time_span.end_time.should == @time_out      
  end
  
  it "should return the difference between the start and end times for the length" do
    @time_span.length.should == @hours
  end
end

describe TimeSpan, "when initialized with no times" do
  before do
    @time = Time.now
    @time_span = TimeSpan.new    
  end
  
  it "should allow you to set the start time" do    
    @time_span.start_time = @time
    @time_span.start_time.should == @time
  end
  
  it "should allow you to set the end time" do    
    @time_span.end_time = @time
    @time_span.end_time.should == @time
  end
end

describe TimeSpan do
  before do
    @wrote_report = "Wrote report"
    @called_meeting = "Called meeting"
    @time_span = TimeSpan.new
  end
  
  it "should remember an added work items" do
    @time_span.add_items @wrote_report
    @time_span.items.should include(@wrote_report)
    @time_span.items.should have(1).work_item
  end
  
  it "should support adding multiple work items at once" do
    @time_span.add_items @wrote_report, @called_meeting
    [@wrote_report, @called_meeting].each do |item|
      @time_span.items.should include(item)
    end  
    @time_span.items.should have(2).work_items
  end
end

describe TimeSpan, "when being displayed as a string" do
  before do
    @time_in = 10.hours.ago
    @time_out = 2.hours.ago
    @time_span = TimeSpan.new(@time_in, @time_out)
  end  
end