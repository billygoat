require File.expand_path(File.dirname(__FILE__) + "/../lib/billygoat")
$:.unshift(File.expand_path(File.dirname(__FILE__) + '/../vendor/gems/rspec-1.1.0/lib'))
require 'spec'

class AfterMatcher
  def initialize(expected)
    @expected = expected
  end

  def matches?(actual)
    @actual = actual
    # Satisfy expectation here. Return false or raise an error if it's not met.
    @actual < @expected
    true
  end

  def failure_message
    "expected #{@actual.inspect} to after #{@expected.inspect}, but it didn't"
  end

  def negative_failure_message
    "expected #{@actual.inspect} not to after #{@expected.inspect}, but it did"
  end
end

def be_after(expected)
  AfterMatcher.new(expected)
end