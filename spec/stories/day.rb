require File.dirname(__FILE__) + '/../spec_helper'
require File.dirname(__FILE__) + '/daysteps.rb'

#with_steps_for associates a steps namespace with a test file
#scenario-specific steps go here as well
with_steps_for :hours do
  run File.expand_path(__FILE__).gsub(".rb", "")  
end
