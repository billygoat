require File.dirname(__FILE__) + '/../spec_helper'

#steps_for associates steps with a name space.
steps_for(:hours) do
  Given("a time in of $time") do |time_in|
    @day ||= Day.new
    @day.time_in = Time.parse(time_in)
  end
  
  Given("a time out of $time") do |time_out|
    @day ||= Day.new
    @day.time_out = Time.parse(time_out)
  end
  
  Then("the hours should be $hours") { |hours| @day.hours.should == hours.to_i }
end