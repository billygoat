require File.dirname(__FILE__) + '/../spec_helper'

describe "A view", :shared => true do    
  it "should be able to format times" do
    @view.should respond_to(:format_time)
  end
  
  it "should be able to format time spans" do
    @view.should respond_to(:format_time_span)
  end
  
  it "should be able to format days" do
    @view.should respond_to(:format_day)
  end
end

describe TextFormatView do
  before do
    @view = TextFormatView.new    
  end
  
  it_should_behave_like "A view"  
end

describe TextFormatView, "when formatting a time" do
  before do
    @view = TextFormatView.new    
    @time_in = Time.parse("09:30")
    @time_out = Time.parse("18:30")
  end  
  
  it "should format time in in 12-hour format" do
    @view.format_time(@time_in).should == "09:30 AM"
  end
  
  it "should format time out in 12-hour format" do
    @view.format_time(@time_out).should == "06:30 PM"
  end
end

describe TextFormatView, "when formatting a time span" do
  before do
    @view = TextFormatView.new
    @time_in = Time.parse("09:30")
    @time_out = Time.parse("18:30")
    @span = TimeSpan.new(@time_in, @time_out)
  end
  
  it "should display time in and time out on separate lines" do
    formatted = @view.format_time_span(@span)
    lines = formatted.split("\n")
    lines.should have_exactly(2).items
  end
end

describe TextFormatView, "when formatting a day" do
  before do
    @view = TextFormatView.new
    @time_in = Time.parse("Fri Nov 09 10:00:00 -0500 2007")
    @time_out = Time.parse("Fri Nov 09 20:00:00 -0500 2007")        
    @items = ["Worked on stuff", "Attended a meeting"]
    
    @day = Day.new
    @day.clock_in!(@time_in)    
    @day.add_items(@items)
    @day.clock_out!@time_out          
  end
  
  it "should format a day beginning with a date header" do
    formatted = @view.format_day(@day)
    formatted.should include('Fri')
  end
end